const express = require('express');
const usersRouter = require('../server/routers/users');
const postsRouter = require('../server/routers/posts');

const app = express();
app.use(express.json());

app.use('/api/users', usersRouter);
app.use('/api/posts', postsRouter);

module.exports = app;
