const { users } = require('../db/models');
const StatusCodes = require('http-status-codes');
const bcrypt = require('bcrypt');
const { signToken } = require('../helpers/validators');
const Users = users;

module.exports = {
	create : async (req, res) => {
		try {
			// check if the User exits
			const exists = await Users.findOne({ where: { username: req.body.username } });
			if (exists) {
				// if the User exists then throw an error
				return res.status(StatusCodes.NOT_ACCEPTABLE).json({ error: 'Username Already Exists' });
			} else {
				//  else create a new User account
				const user = await Users.create(req.body);
				console.log('User Account Created', user);
				// create an acces token using JWT
				const token = signToken(user);
				// return the Token to the user
				return res.status(StatusCodes.OK).json({ message: 'New User Successfully Created', token: token });
			}
		} catch (error) {
			// throw an error in case the user account creation fails
			console.log('Error creating User Account', error);
			return res.status(StatusCodes.EXPECTATION_FAILED).json({ error: 'User Account Creation Failed' });
		}
	},
	login  : async (req, res) => {
		try {
			// check whether the user in the Data base
			const user = await Users.findOne({ where: { username: req.body.username } });
			if (!user) {
				console.log('LogIn Failed, User does not Exist!');
				return res.status(StatusCodes.NOT_FOUND).json({ error: 'Login Failed!, User doesnot Exist' });
			} else {
				const match = await bcrypt.compare(req.body.password, user.password);
				if (match) {
					console.log('Login Successfully Password matched User record!');
					const token = signToken(user);
					return res.status(StatusCodes.OK).json({ token });
				} else {
					console.log('Login Failed, password didnot match user record!');
					return res.status(StatusCodes.NOT_FOUND).json({ error: 'Login Failed! Wrong Username/Password' });
				}
				// return res.status(StatusCodes.OK).json({ userDetails: user });
			}
		} catch (error) {
			console.log('Error! Failed to login');
			return res.status(StatusCodes.EXPECTATION_FAILED).json({ error: 'Error!, Failed to Login' });
		}
	}
};
