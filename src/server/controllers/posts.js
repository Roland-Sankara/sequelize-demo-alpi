const { posts } = require('../db/models');
const StatusCodes = require('http-status-codes');
const Posts = posts;

module.exports = {
	createPost : async (req, res) => {
		try {
			const post = await Posts.create({
				user_id : req.userID,
				comment : req.body.comment
			});
			console.log('New Post has been created!');
			return res.status(StatusCodes.OK).json({ message: 'new post Successfully created!', Post: post });
		} catch (error) {
			console.log('Failed to create Post!');
			return res.status(StatusCodes.EXPECTATION_FAILED).json({ error: 'Failed to create Post!' });
		}
	},
	getPosts   : async (req, res) => {
		try {
			const posts = await Posts.findAll({
				attributes : [ 'user_id', 'comment' ]
			});
			if (posts.length > 0) {
				return res.status(StatusCodes.OK).json({ Posts: posts });
			} else {
				console.log('Failed to Find Posts/Posts Array Empty');
				return res.status(StatusCodes.NOT_FOUND).json({ error: 'No Posts Found' });
			}
		} catch (error) {
			console.log('Failed to Find Posts!');
			return res.status(StatusCodes.EXPECTATION_FAILED).json({ error: 'Failed to Find Posts!' });
		}
	}
};
