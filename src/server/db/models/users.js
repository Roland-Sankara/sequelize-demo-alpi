'use strict';
const bcrypt = require('bcrypt')
module.exports = (sequelize, DataTypes) => {
	const users = sequelize.define(
		'users',
		{
			username : DataTypes.STRING,
			first_name  : DataTypes.STRING,
			password   : DataTypes.STRING
		},
		{
			tableName     : 'users',
			setterMethods : {
				password(value) {
					this.setDataValue('password', bcrypt.hashSync(value, 10));
				}
			}
		}
	);
	users.associate = function(models) {
		// associations can be defined here
	};
	return users;
};
