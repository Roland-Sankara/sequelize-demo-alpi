'use strict';
module.exports = (sequelize, DataTypes) => {
	const posts = sequelize.define(
		'posts',
		{
			user_id : DataTypes.INTEGER,
			comment : DataTypes.STRING
		},
		{
			tableName : 'posts'
		}
	);
	posts.associate = function(models) {
		// associations can be defined here
	};
	return posts;
};
