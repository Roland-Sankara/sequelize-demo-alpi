require('dotenv').config();
const JWT = require('jsonwebtoken');
const StatusCodes = require('http-status-codes');

module.exports = {
	authenticate : (req, res, next) => {
		// Get for authorization in the headers object
		const header = req.headers['authorization'];
		if (!header) {
			return res.status(StatusCodes.PARTIAL_CONTENT).json({ error: 'No Token Specified!' });
		}
		// if the auth header is present then capture the token else return an error
		if (header.startsWith('Bearer')) {
			let token = header.split(' ')[1];
			//if you have got the token, now verify it with JWT
			JWT.verify(token, process.env.JWT_SECRET, (error, userData) => {
				if (error) {
					return res.status(StatusCodes.NOT_ACCEPTABLE).json({
						error : 'Provided Token is Invalid/Expired !'
					});
				} else {
					// add the user id to the request object
					req.userID = userData.id;
					next();
				}
			});
		} else {
			return res.status(StatusCodes.NOT_ACCEPTABLE).json({ error: 'Provided Token is in an Invalid Token' });
		}
	},
	signToken    : (user) => {
		return JWT.sign(
			{
				id : user.id
			},
			process.env.JWT_SECRET,
			{
				expiresIn : '24h'
			}
		);
	}
};
