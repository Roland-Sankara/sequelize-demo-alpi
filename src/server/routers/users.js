const router = require('express-promise-router')();

const { create, login } = require('../controllers/users');

router.route('/signup').post(create);
router.route('/login').post(login)

module.exports = router