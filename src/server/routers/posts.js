const router = require('express-promise-router')();
const { createPost, getPosts } = require('../controllers/posts');
// Import te validators
const { authenticate } = require('../helpers/validators');

// routes
router.route('/create').post(authenticate,createPost);
router.route('/').get(authenticate, getPosts);

module.exports = router;
