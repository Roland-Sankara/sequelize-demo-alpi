require('dotenv').config();
const app = require('./src/server/app');

const port = 3000;

app.listen(port, () => {
	console.log(`Node Environment: ${process.env.NODE_ENV}`);
	console.log(`Server Started and listening on Port ${port}`);
});
